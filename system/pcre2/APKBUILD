# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Zach van Rijn <me@zv.io>
pkgname=pcre2
pkgver=10.42
pkgrel=0
pkgdesc="Perl-compatible regular expression library"
url="https://pcre.org"
arch="all"
license="BSD-3-Clause"
depends=""
depends_dev="libedit-dev zlib-dev"
makedepends="$depends_dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-tools
	libpcre2-16:_libpcre libpcre2-32:_libpcre"
source="https://github.com/PhilipHazel/pcre2/releases/download/$pkgname-$pkgver/$pkgname-$pkgver.tar.gz
	"

# secfixes:
#   10.33-r1:
#     - CVE-2019-20454
#   10.35-r0:
#     - CVE-2019-20454

case "$CARCH" in
	s390x) _enable_jit="";;
	pmmx) _enable_jit="";;  # maybe someday fix sse2 detection
	*) _enable_jit="--enable-jit";;
esac

build() {
	# Note: Forced -O3 is recommended (needed?) for Julia.
	./configure \
		CFLAGS="$CFLAGS -O3" \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--docdir=/usr/share/doc/$pkgname-$pkgver \
		--htmldir=/usr/share/doc/$pkgname-$pkgver/html \
		--enable-pcre2-16 \
		--enable-pcre2-32 \
		--enable-pcre2grep-libz \
		--enable-pcre2test-libedit \
		--with-match-limit-recursion=8192 \
		$_enable_jit
	make
}

check() {
	./RunTest
	[ ! -n "$_enable_jit" ] || ./pcre2_jit_test
}

package() {
	make DESTDIR="$pkgdir" install
}

_libpcre() {
	local bits="${subpkgname##*-}"
	pkgdesc="PCRE2 with $bits bit character support"

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libpcre2-$bits.so* "$subpkgdir"/usr/lib/
}

tools() {
	pkgdesc="Auxiliary utilities for PCRE2"

	mkdir -p "$subpkgdir"/usr/
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

sha512sums="a3db6c5c620775838819be616652e73ce00f5ef5c1f49f559ff3efb51a119d02f01254c5901c1f7d0c47c0ddfcf4313e38d6ca32c35381b8f87f36896d10e6f7  pcre2-10.42.tar.gz"
