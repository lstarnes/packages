# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=musl
pkgver=1.2.3
pkgrel=1
pkgdesc="System library (libc) implementation"
url="https://www.musl-libc.org/"
arch="all"
options="!check"
license="MIT"
depends=""
makedepends=""
subpackages="$pkgname-dev"
case "$BOOTSTRAP" in
nocc)	pkgname="musl-dev"
	subpackages=""
	options="$options !dbg"
	builddir="$srcdir"/musl-$pkgver
	;;
nolibc) ;;
*)	subpackages="$subpackages $pkgname-utils"
	triggers="$pkgname-utils.trigger=/etc/ld.so.conf.d"
	;;
esac
source="https://musl.libc.org/releases/musl-${pkgver}.tar.gz
	amalgamation.patch
	3001-make-real-lastlog-h.patch
	handle-aux-at_base.patch
	fgetspent_r.patch
	realpath.patch
	signed-wchar_t-fixes.patch

	ldconfig
	getent.c
	iconv.c
	"

# secfixes:
#   1.1.15-r4:
#     - CVE-2016-8859
#   1.1.23-r2:
#     - CVE-2019-14697
#   1.2.0-r2:
#     - CVE-2020-28928

build() {
	[ "$BOOTSTRAP" = "nocc" ] && return 0

	if [ "$BOOTSTRAP" != "nolibc" ]; then
		# getconf/getent/iconv
		local i
		for i in getent iconv ; do
			${CROSS_COMPILE}gcc $CPPFLAGS $CFLAGS "$srcdir"/$i.c -o $i
		done
	fi

	# note: not autotools
	LDFLAGS="$LDFLAGS -Wl,-soname,libc.musl-${CARCH}.so.1" \
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var
	make
}

package() {
	if [ "$BOOTSTRAP" = "nocc" ]; then
		case "$CARCH" in
		aarch64*)	ARCH="aarch64" ;;
		arm*)		ARCH="arm" ;;
		x86)		ARCH="i386" ;;
		x86_64)		ARCH="x86_64" ;;
		ppc)		ARCH="powerpc" ;;
		ppc64*)		ARCH="powerpc64" ;;
		s390*)		ARCH="s390x" ;;
		mips64*)	ARCH="mips64" ;;
		mips*)		ARCH="mips" ;;
		m68k)		ARCH="m68k" ;;
		esac

		make ARCH="$ARCH" prefix=/usr DESTDIR="$pkgdir" install-headers
	else
		make DESTDIR="$pkgdir" install

		# make LDSO the be the real file, and libc the symlink
		local LDSO="$(make -f Makefile --eval "$(printf 'print-ldso:\n\t@echo $$(basename $(LDSO_PATHNAME))')" print-ldso)"
		mv -f "$pkgdir"/usr/lib/libc.so "$pkgdir"/lib/"$LDSO"
		ln -sf "$LDSO" "$pkgdir"/lib/libc.musl-${CARCH}.so.1
		ln -sf ../../lib/"$LDSO" "$pkgdir"/usr/lib/libc.so
		mkdir -p "$pkgdir"/usr/bin
		ln -sf ../../lib/"$LDSO" "$pkgdir"/usr/bin/ldd
	fi

	rm "$pkgdir"/usr/include/utmp.h     # utmps
	rm "$pkgdir"/usr/include/utmpx.h    # utmps
}

dev() {
	provides="libc-dev=$pkgver-r$pkgrel"
	default_dev
}

utils() {
	depends="!uclibc-utils scanelf"
	provides="libc-utils=$pkgver-r$pkgrel"
	replaces="libiconv uclibc-utils"
	license="BSD-2-Clause AND GPL-2.0+"

	mkdir -p "$subpkgdir"/usr "$subpkgdir"/sbin
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/

	install -D \
		"$builddir"/getent \
		"$builddir"/iconv \
		"$subpkgdir"/usr/bin

	install -D -m755 "$srcdir"/ldconfig "$subpkgdir"/sbin
}

sha512sums="9332f713d3eb7de4369bc0327d99252275ee52abf523ee34b894b24a387f67579787f7c72a46cf652e090cffdb0bc3719a4e7b84dca66890b6a37f12e8ad089c  musl-1.2.3.tar.gz
f7b05d8c5f804ba3ad6998b3de5fa4d9dfceac4aca63dd67298c2d5f27cdd28a91eba74f6e428c258323da80635dd333bae2c47ff918894797cba92bd5700909  amalgamation.patch
88ae443dbb8e0a4368235bdc3a1c5c7b718495afa75e06deb8e01becc76cb1f0d6964589e2204fc749c9c1b3190b8b9ac1ae2c0099cab8e2ce3ec877103d4332  3001-make-real-lastlog-h.patch
1f4e9aea5a546015c75f77aa0dec10d56fc14831ccc15cf71ff27fc15ac5230ffeadb382ebe1c87c1ea07a462620e16ed01cd36252d997d1a9c2af11cb5c9ff3  handle-aux-at_base.patch
ded41235148930f8cf781538f7d63ecb0c65ea4e8ce792565f3649ee2523592a76b2a166785f0b145fc79f5852fd1fb1729a7a09110b3b8f85cba3912e790807  fgetspent_r.patch
d5ec3f1a86f2194e0af83c2391508811b939d0f8f2fd2ac5ac7f03774f8a250ce42399110d2ae04d32b864ee292863fed683a029b64598dbbcb21d9811a825d0  realpath.patch
3770af3bc961e5d5b8c152c428cd20dc54e026b23b31d764fbc2e71ee38140d160db2267755f23800bc8586fd4b51554b1caebb2415bef82fd0f4a6dd8bf640d  signed-wchar_t-fixes.patch
cb71d29a87f334c75ecbc911becde7be825ab30d8f39fa6d64cb53812a7c9abaf91d9804c72540e5be3ddd3c84cfe7fd9632274309005cb8bcdf9a9b09b4b923  ldconfig
378d70e65bcc65bb4e1415354cecfa54b0c1146dfb24474b69e418cdbf7ad730472cd09f6f103e1c99ba6c324c9560bccdf287f5889bbc3ef0bdf0e08da47413  getent.c
9d42d66fb1facce2b85dad919be5be819ee290bd26ca2db00982b2f8e055a0196290a008711cbe2b18ec9eee8d2270e3b3a4692c5a1b807013baa5c2b70a2bbf  iconv.c"
