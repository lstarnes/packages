# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=attr
pkgver=2.5.1
pkgrel=0
pkgdesc="Utilities for managing filesystem extended attributes"
url="https://savannah.nongnu.org/projects/attr"
arch="all"
license="GPL-2.0+ AND LGPL-2.1+"
depends=""
makedepends="slibtool autoconf automake bash gettext-tiny gettext-tiny-dev"
checkdepends="perl"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang libattr"
source="https://download.savannah.nongnu.org/releases/attr/attr-$pkgver.tar.gz
	test-runner-musl.patch
	"

prepare() {
	default_prepare
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--exec-prefix=/ \
		--sbindir=/sbin \
		--libdir=/lib \
		--includedir=/usr/include \
		--mandir=/usr/share/man \
		--datadir=/usr/share \
		--sysconfdir=/etc
	make
}

check() {
	make check
}

package() {
	make -j1 DESTDIR="$pkgdir" install
}

libattr() {
	pkgdesc="Dynamic library for extended attribute support"
	replaces="attr"
	mkdir -p "$subpkgdir"/lib
	mv "$pkgdir"/lib/lib*.so.* "$subpkgdir"/lib/
}

sha512sums="8b4c043d61f8f3e0cd098e701181069f51117b85fd6ba18bfe9af77d894ea671232377d4793ffc822e9259ceee6ac71d75732eb93b2830c6cb5d0d918ba2d21b  attr-2.5.1.tar.gz
da4b903ae0ba1c72bae60405745c1135d1c3c1cefd7525fca296f8dc7dac1e60e48eeba0ba80fddb035b24b847b00c5a9926d0d586c5d7989d0428e458d977d3  test-runner-musl.patch"
