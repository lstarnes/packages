# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libedit
pkgver=20210910.3.1
_realver="${pkgver%%.*}-${pkgver#*.}"
pkgrel=0
pkgdesc="Library providing line editing, history, and tokenisation functions"
url="https://thrysoee.dk/editline/"
arch="all"
license="BSD-2-Clause"
depends=""
depends_dev="ncurses-dev"  # XXX is this always unconditionally needed?
makedepends="$depends_dev autoconf automake libtool"
subpackages="$pkgname-dev $pkgname-doc"
source="https://thrysoee.dk/editline/libedit-$_realver.tar.gz"
builddir="$srcdir/$pkgname-$_realver"

prepare() {
	default_prepare
	autoreconf -v -f --install
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="b7361c277f971ebe87e0e539e5e1fb01a4ca1bbab61e199eb97774d8b60dddeb9e35796faf9cc68eb86d1890e8aac11db13b44b57ccc8302d559741fbe9d979e  libedit-20210910-3.1.tar.gz"
