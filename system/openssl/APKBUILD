# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=openssl
pkgver=1.1.1v
pkgrel=0
pkgdesc="Toolkit for SSL and TLS"
url="https://www.openssl.org/"
arch="all"
license="OpenSSL"
depends=""
checkdepends="perl"
makedepends_build="perl"
subpackages="$pkgname-dbg $pkgname-dev $pkgname-doc libcrypto1.1:libcrypto
	libssl1.1:libssl"
source="https://www.openssl.org/source/${pkgname}-${pkgver}.tar.gz
	ppc-auxv.patch
	ppc64.patch
	"

# secfixes:
#   1.0.2h-r0:
#     - CVE-2016-2107
#     - CVE-2016-2105
#     - CVE-2016-2106
#     - CVE-2016-2109
#     - CVE-2016-2176
#   1.0.2h-r1:
#     - CVE-2016-2177
#     - CVE-2016-2178
#   1.0.2h-r2:
#     - CVE-2016-2180
#   1.0.2h-r3:
#     - CVE-2016-2179
#     - CVE-2016-2182
#     - CVE-2016-6302
#     - CVE-2016-6303
#   1.0.2h-r4:
#     - CVE-2016-2181
#   1.0.2i-r0:
#     - CVE-2016-2183
#     - CVE-2016-6304
#     - CVE-2016-6306
#   1.0.2m-r0:
#     - CVE-2017-3735
#     - CVE-2017-3736
#   1.0.2n-r0:
#     - CVE-2017-3737
#     - CVE-2017-3738
#   1.0.2o-r0:
#     - CVE-2018-0739
#     - CVE-2018-0737
#     - CVE-2018-0732
#   1.0.2r-r0:
#     - CVE-2018-0734
#     - CVE-2018-5407
#     - CVE-2019-1559
#   1.0.2t-r0:
#     - CVE-2019-1547
#     - CVE-2019-1563
#   1.1.1d-r0:
#     - CVE-2019-1551
#   1.1.1g-r0:
#     - CVE-2020-1967
#   1.1.1t-r1:
#     - CVE-2023-0465

build() {
	# openssl will prepend crosscompile always core CC et al
	CC=${CC#${CROSS_COMPILE}}
	CXX=${CXX#${CROSS_COMPILE}}
	CPP=${CPP#${CROSS_COMPILE}}

	# determine target OS for openssl
	case "$CARCH" in
	aarch64*) _target="linux-aarch64" ;;
	arm*)   _target="linux-armv4" ;;
	ppc)	_target="linux-ppc" ;;
	ppc64)	_target="linux-ppc64" ;;
	ppc64le) _target="linux-ppc64le" ;;
	i528 | pmmx | x86) _target="linux-elf" ;;
	x86_64) _target="linux-x86_64" ;;
	s390x)	_target="linux64-s390x";;
	*)	msg "Unable to determine architecture from (CARCH=$CARCH)" ; return 1 ;;
	esac

	# Configure assumes --options are for it, so can't use
	# gcc's --sysroot fake this by overriding CC
	[ -n "$CBUILDROOT" ] && CC="$CC --sysroot=${CBUILDROOT}"

	perl ./Configure $_target --prefix=/usr \
		--libdir=lib \
		--openssldir=/etc/ssl \
		shared no-zlib enable-md2 no-weak-ssl-ciphers \
		$CPPFLAGS $CFLAGS $LDFLAGS -Wa,--noexecstack
	make
}

check() {
	make test
}

package() {
	make DESTDIR="$pkgdir" install
	rm "$pkgdir"/usr/bin/c_rehash

	for _manfile in "$pkgdir"/usr/share/man/man1/*; do
		case _manfile in
		openssl*) ;;
		*) mv $_manfile "$pkgdir"/usr/share/man/man1/openssl-$(basename $_manfile)
		esac
	done
}

libcrypto() {
	pkgdesc="OpenSSL cryptography library"

	mkdir -p "$subpkgdir"/lib "$subpkgdir"/usr/lib
	for i in "$pkgdir"/usr/lib/libcrypto*; do
		mv $i "$subpkgdir"/lib/
		ln -s ../../lib/${i##*/} "$subpkgdir"/usr/lib/${i##*/}
	done
	mv "$pkgdir"/usr/lib/engines-1.1 "$subpkgdir"/usr/lib/
}

libssl() {
	pkgdesc="OpenSSL socket library"

	mkdir -p "$subpkgdir"/lib "$subpkgdir"/usr/lib
	for i in "$pkgdir"/usr/lib/libssl*; do
		mv $i "$subpkgdir"/lib/
		ln -s ../../lib/${i##*/} "$subpkgdir"/usr/lib/${i##*/}
	done
}

sha512sums="1a67340d99026aa62bf50ff89165d9f77fe4a6690fe30d1751b5021dd3f238391afd581b41724687c322c4e3af1770c44a63766a06e9b8cab6425101153e0c7e  openssl-1.1.1v.tar.gz
7fd3158c6eb3451f10e4bfd78f85c3e7aef84716eb38e00503d5cfc8e414b7bdf02e0671d0299a96a453dd2e38249dcf1281136b27b6df372f3ea08fbf78329b  ppc-auxv.patch
e040f23770d52b988578f7ff84d77563340f37c026db7643db8e4ef18e795e27d10cb42cb8656da4d9c57a28283a2828729d70f940edc950c3422a54fea55509  ppc64.patch"
