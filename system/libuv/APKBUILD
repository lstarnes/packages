# Contributor: Jose-Luis Rivas <ghostbar@riseup.net>
# Conttributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libuv
pkgver=1.44.2
pkgrel=0
pkgdesc="Cross-platform asychronous I/O"
url="https://libuv.org"
arch="all"
license="MIT AND BSD-2-Clause AND BSD-3-Clause AND ISC"
makedepends="automake autoconf libtool linux-headers"
subpackages="$pkgname-dev"
source="https://dist.libuv.org/dist/v$pkgver/$pkgname-v$pkgver.tar.gz
	fix-test-tty-size-assumption.patch
	"
builddir="$srcdir/$pkgname-v$pkgver"

prepare() {
	default_prepare
	sh autogen.sh
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make CFLAGS="$CFLAGS -D__USE_MISC" BUILDTYPE=Release
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	install -Dm644 LICENSE \
		"$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}

sha512sums="91197ff9303112567bbb915bbb88058050e2ad1c048815a3b57c054635d5dc7df458b956089d785475290132236cb0edcfae830f5d749de29a9a3213eeaf0b20  libuv-v1.44.2.tar.gz
e7b2242345af697dd175a5fc29b7eb001e5b0f43743a86d9e3fc7307cf3b3bb98e46c2993e84332ecb2934a30f942a78f4ddd19efed6871d85fcf8e2bba15643  fix-test-tty-size-assumption.patch"
