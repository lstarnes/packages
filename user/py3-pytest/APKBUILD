# Contributor: Max Rees <maxcrees@me.com>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=py3-pytest
_pkgname=pytest
pkgver=6.2.5
pkgrel=1
pkgdesc="Python test library"
url="https://pypi.python.org/pypi/pytest"
arch="noarch"
options="!check"  # Cyclic dependency with itself. Passes on x86_64
# Certified net clean
license="MIT"
depends="python3 py3-atomicwrites py3-attrs py3-iniconfig py3-more-itertools
	py3-packaging py3-pluggy py3-py py3-six py3-wcwidth"
makedepends="python3-dev"
checkdepends="py3-pytest py3-hypothesis"
source="$pkgname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/p/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	python3 setup.py build
}

check() {
	rm -f testing/test_junitxml.py
	# You would think you could get away with "python3 -m pytest" but no...
	PYTHONPATH="$builddir/build/lib:$PYTHONPATH" pytest
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="7624563a9d967da4cbf82cfff90bae8c0cca07b32e291dc7c5efa787725ed1a255edd066bf0d5fbd89b8cbed8cf5b619fe7c7017f44a7f8a014e3310c06bdbf9  py3-pytest-6.2.5.tar.gz"
