# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kactivities-stats
pkgver=5.94.0
pkgrel=0
pkgdesc="Gather statistics about KDE activities"
url="https://api.kde.org/frameworks/kactivities/html/index.html"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.1-only OR LGPL-3.0-only"
depends=""
depends_dev="kactivities-dev"
checkdepends="boost-dev"
docdepends="kactivities-doc"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qtbase-dev $docdepends
	qt5-qtdeclarative-dev qt5-qttools-dev doxygen graphviz kconfig-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kactivities-stats-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} -Bbuild
	make -C build
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="0ae5d4ca81ef58b2d4857f737436eaed85bc50da910d7f1dc348e972c8511862b67869bd85a577eb43c06d8cd4e796092a90cb6ec1cb83013e76175643bc5cf5  kactivities-stats-5.94.0.tar.xz"
