# Contributor: Peter Bui <pnutzh4x0r@gmail.com>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Timo Teräs <timo.teras@iki.fi>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=youtube-dl
pkgver=2020.07.28
pkgrel=0
pkgdesc="Command-line program to download videos from YouTube and many other sites"
url="https://youtube-dl.org"
arch="noarch"
options="!check"  # Takes way too long, and uses network
license="Unlicense AND Public-Domain"
depends="ffmpeg python3"
makedepends=""
subpackages="$pkgname-doc
	$pkgname-zsh-completion:zshcomp
	$pkgname-bash-completion:bashcomp
	$pkgname-fish-completion:fishcomp"
source="https://github.com/rg3/$pkgname/releases/download/$pkgver/$pkgname-$pkgver.tar.gz
	tumblr.patch
	"
builddir="$srcdir/$pkgname"

prepare() {
	default_prepare
	sed -i \
		-e 's|etc/bash_completion.d|share/bash-completion/completions|' \
		-e 's|etc/fish/completions|share/fish/completions|' \
		"$builddir/setup.py"
}

check() {
	python3 -m unittest discover
}

package() {
	python3 setup.py install --root="$pkgdir/" --optimize=1
}

zshcomp() {
	pkgdesc="Zsh completions for $pkgname"
	depends=""
	install_if="$pkgname=$pkgver-r$pkgrel zsh"

	install -Dm644 "$builddir/$pkgname.zsh" \
		"$subpkgdir/usr/share/zsh/site-functions/_$pkgname"
}

bashcomp() {
	pkgdesc="Bash completions for $pkgname"
	depends=""
	install_if="$pkgname=$pkgver-r$pkgrel bash-completion"

	mkdir -p "$subpkgdir/usr/share/bash-completion/completions/"
	mv "$pkgdir/usr/share/bash-completion/completions/$pkgname.bash-completion" \
		"$subpkgdir/usr/share/bash-completion/completions/$pkgname"
}

fishcomp() {
	pkgdesc="Fish completions for $pkgname"
	depends=""
	install_if="$pkgname=$pkgver-r$pkgrel fish"

	mkdir -p "$subpkgdir/usr/share/fish/completions/"
	mv "$pkgdir/usr/share/fish/completions/$pkgname.fish" \
		"$subpkgdir/usr/share/fish/completions/"
}

sha512sums="be18cd53577a1e750a9610d481225b5683414ee4a095aa90b1a9ef150e9009bec4c2188f19f13505c88ac0179872751a07f5fb4b591beca3cefd11ccf071132d  youtube-dl-2020.07.28.tar.gz
5760d06e6bbc1eee2c6be2d1f580f86b3cfa5f4bc44a62fb8145ce1cd41352ecf2f65d65d79a2d7f1ec129a34c28a7ec3d0d328c907e743bfcea54c65c71285d  tumblr.patch"
