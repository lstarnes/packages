# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=irssi
pkgver=1.2.3
pkgrel=0
pkgdesc="Text-based IRC client"
url="https://irssi.org"
arch="all"
license="GPL-2.0+ AND ISC"
depends=""
makedepends="ncurses-dev glib-dev openssl-dev perl-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-perl"
source="https://github.com/irssi/irssi/releases/download/$pkgver/irssi-$pkgver.tar.xz"

# secfixes: irssi
#   1.2.1-r0:
#     - CVE-2019-13045
#   1.2.2-r0:
#     - CVE-2019-15717

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--enable-true-color \
		--with-perl=module \
		--with-perl-lib=vendor
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

perl() {
	depends="perl"
	pkgdesc="Perl support & scripts for irssi"

	mkdir -p "$subpkgdir"/usr "$subpkgdir"/usr/share/irssi
	mv "$pkgdir"/usr/lib "$subpkgdir"/usr
	mv "$pkgdir"/usr/share/irssi/scripts "$subpkgdir"/usr/share/irssi
}
sha512sums="826b7bfd86a54647f2d344b6c461e1118b7382fb1637cf33c395af41a9a4ca5d8a794a415f0f0737178968cf2463bb46a0e7b7fd7014c968668b16183e0644bc  irssi-1.2.3.tar.xz"
