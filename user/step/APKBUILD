# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=step
pkgver=22.04.2
pkgrel=0
pkgdesc="Interactive physics simulation"
url="https://www.kde.org/applications/education/step/"
arch="all"
license="GPL-2.0-only"
depends="shared-mime-info"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev
	kdoctools-dev qt5-qtsvg-dev kcrash-dev khtml-dev kconfig-dev eigen-dev
	kdelibs4support-dev knewstuff-dev kplotting-dev gsl-dev
	libqalculate-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/step-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DEIGEN3_INCLUDE_DIR=/usr/include/eigen3 \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="329fdfd99f898bfed3f783a86fdfd305c55a925910c5c5ea66ea90ddf107f808815ada604aa1069d0ce40c3bdafb46ea3f1b253675ec9bcbd7dc55e3856a2552  step-22.04.2.tar.xz"
