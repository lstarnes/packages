# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Síle Ekaterin Liszka <sheila@adelielinux.org> 
pkgname=nghttp2-tools
pkgver=1.52.0
pkgrel=0
pkgdesc="Experimental HTTP/2 client, server and proxy"
url="https://nghttp2.org/"
arch="all"
license="MIT"
depends="nghttp2=$pkgver-r$pkgrel"
checkdepends="cunit-dev"
makedepends="c-ares-dev jansson-dev libev-dev libxml2-dev openssl-dev zlib-dev"
subpackages="$pkgname-doc"
source="https://github.com/tatsuhiro-t/nghttp2/releases/download/v$pkgver/nghttp2-$pkgver.tar.xz"
builddir="$srcdir/nghttp2-$pkgver"

# secfixes:
#   1.41.0-r0:
#     - CVE-2020-11080

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--disable-static \
		--without-neverbleed \
		--without-jemalloc \
		--enable-app \
		--disable-shared
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install

	# duplicate of -doc
	rm -rf "$pkgdir"/usr/share/doc
	# duplicate of -dev
	rm -rf "$pkgdir"/usr/include
	rm -rf "$pkgdir"/usr/lib
	# duplicate of nghttp2
	rm -rf "$pkgdir"/usr/share/nghttp2
}

sha512sums="3af1ce13270f7afc8652bd3de71200d9632204617fe04d2be7156d60eeb1a5cc415573677791a399ae03577e8e3256939b1b05d27dbd98dee504d09ec5325d56  nghttp2-1.52.0.tar.xz"
