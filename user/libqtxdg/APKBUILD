# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=libqtxdg
pkgver=3.12.0
_lxqt=0.13.0
pkgrel=0
pkgdesc="Qt5-based library implementing the XDG spec"
url="https://lxqt.github.io/"
arch="all"
options="!check"  # Test suite requires X11.
license="LGPL-2.1+"
depends=""
makedepends="cmake extra-cmake-modules lxqt-build-tools>=$_lxqt qt5-qtbase-dev qt5-qtsvg-dev"
subpackages="$pkgname-dev"
source="https://github.com/lxqt/libqtxdg/releases/download/$pkgver/libqtxdg-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_TESTS=True \
		-DBUILD_DEV_UTILS=True \
		${CMAKE_CROSSOPTS} -Bbuild
	make -C build
}

check() {
	make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="9db46b0dddb027a24e1c19a2e3679014735cc246f15d32bc5fb9e778cd7d8fb57c9b105f819091308002f0a2eff8269920ecc0d7bd5ce8a01fbaf0b37e61a76e  libqtxdg-3.12.0.tar.xz"
