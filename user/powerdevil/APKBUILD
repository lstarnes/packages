# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox-kde@adelielinux.org>
pkgname=powerdevil
pkgver=5.24.5
pkgrel=0
pkgdesc="KDE Plasma power management utilities"
url="https://www.kde.org/"
arch="all"
license="GPL-2.0+ AND LGPL-2.0+ AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends="kirigami2"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtx11extras-dev
	kactivities-dev kauth-dev kconfig-dev kdbusaddons-dev kglobalaccel-dev
	ki18n-dev kidletime-dev kio-dev knotifyconfig-dev kdelibs4support-dev
	kwayland-dev libkscreen-dev libkworkspace-dev solid-dev eudev-dev
	bluez-qt-dev kirigami2-dev libcap-dev networkmanager-qt-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/powerdevil-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="2f3e6888be7039bf7beb754ef117d09e79ceb4e5ef73d74f982d37785adbd7a489fc86641c9df9ea4a34fabd2ccac737f5e7ee08958d2dd03b5a676f22aa4c15  powerdevil-5.24.5.tar.xz"
