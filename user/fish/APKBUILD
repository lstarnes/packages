# Contributor: Ariadne Conill <ariadne@dereferenced.org>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=fish
pkgver=3.4.1
pkgrel=0
pkgdesc="Modern interactive commandline shell"
url="http://www.fishshell.com"
arch="all"
license="BSD-3-Clause AND BSD-2-Clause AND GPL-2.0+ AND GPL-2.0-only AND ISC"
depends="bc groff"
depends_dev="$pkgname-tools"
checkdepends="py3-pexpect"
makedepends="cmake doxygen ncurses-dev pcre2-dev"
install="$pkgname.post-install $pkgname.post-upgrade $pkgname.pre-deinstall"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang $pkgname-tools::noarch"
source="https://github.com/fish-shell/fish-shell/releases/download/$pkgver/$pkgname-$pkgver.tar.xz"

build() {
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_SYSCONFDIR=/etc \
		-DCURSES_NEED_NCURSES=ON \
		-Bbuild .
	make -C build
}

check() {
	make -C build test
}

package() {
	make -C build install DESTDIR="$pkgdir"
}

dev() {
	default_dev

	mkdir -p "$subpkgdir"/usr/share
	mv "$pkgdir"/usr/share/pkgconfig "$subpkgdir"/usr/share
}

doc() {
	default_doc

	mkdir -p "$subpkgdir"/usr/share/$pkgname
	mv "$pkgdir"/usr/share/$pkgname/man "$subpkgdir"/usr/share/$pkgname
}

tools() {
	pkgdesc="$pkgdesc (tools)"
	depends="$pkgname python3"

	mkdir -p "$subpkgdir"/usr/share/$pkgname
	mv "$pkgdir"/usr/share/$pkgname/tools "$subpkgdir"/usr/share/$pkgname
}

sha512sums="20a2892ec0c413c4c3fcfe5fbf52fb2398de35a9172758728bd2ccdccc5fb6e0e18712a664d02db67543d47180a4d04f3998a6297d23088926b6d03baefdf981  fish-3.4.1.tar.xz"
