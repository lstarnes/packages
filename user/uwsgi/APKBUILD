# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=uwsgi
pkgver=2.0.21
pkgrel=0
pkgdesc="Web application server"
url="https://projects.unbit.it/uwsgi"
arch="all"
options="!check"  # Requires unpackaged `cppcheck`.
license="GPL-2.0-only"
depends=""
makedepends="libxml2-dev openssl-dev zlib-dev
	perl-dev php7-dev php7-embed python3-dev ruby-dev"
subpackages=""
# Keep psgi at the end for the CFLAGS hack.
_plugins="asyncio cgi corerouter http logfile php python rack psgi"
for _plugin in $_plugins; do
	subpackages="$subpackages $pkgname-$_plugin"
done
source="$pkgname-$pkgver.tar.gz::https://github.com/unbit/uwsgi/archive/refs/tags/$pkgver.tar.gz"

build() {
	echo 'plugin_dir = /usr/lib/uwsgi/plugins' >> buildconf/core.ini
	echo 'plugin_build_dir = .' >> buildconf/core.ini

	python3 uwsgiconfig.py --build core

	for _plugin in $_plugins; do
		[ $_plugin != "psgi" ] || export CFLAGS="-D_GNU_SOURCE -include /usr/include/setjmp.h"
		python3 uwsgiconfig.py --plugin plugins/$_plugin core
	done
}

check() {
	python3 uwsgiconfig.py --check
}

package() {
	install -D -m755 "$builddir"/uwsgi "$pkgdir"/usr/bin/uwsgi
}

_plugpack() {
	pkgdesc="$pkgdesc ($2 plugin)"
	depends=""

	install -D -m755 "$builddir"/$1_plugin.so \
		"$subpkgdir"/usr/lib/uwsgi/plugins/$1_plugin.so
}

asyncio() {
	_plugpack asyncio "Python asyncio"
}

cgi() {
	_plugpack cgi "CGI"
}

corerouter() {
	_plugpack corerouter "Core router"
}

http() {
	_plugpack http "HTTP server"
}

logfile() {
	_plugpack logfile "Log file"
}

php() {
	_plugpack php "PHP"
}

psgi() {
	_plugpack psgi "PSGI"
}

python() {
	_plugpack python "Python 3"
}

rack() {
	_plugpack rack "Ruby Rack"
}

sha512sums="36a9c1b87a4c3d08482b9045d2227f56b006acb53f38ddf1b510880ae5fc24c0177a077338ec8af3ef0b8f8e220bc4fc7f8311dab8066e13cbcbb616f736c795  uwsgi-2.0.21.tar.gz"
