# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Maintainer: 
pkgname=mdadm
pkgver=4.1
pkgrel=2
pkgdesc="Tool for managing Linux soft RAID arrays"
url="http://neil.brown.name/blog/mdadm"
arch="all"
options="!check"  # No test suite.
license="GPL-2.0-only"
depends=""
makedepends="groff linux-headers"
subpackages="$pkgname-doc $pkgname-misc::noarch $pkgname-openrc
	$pkgname-udev::noarch"
source="https://mirrors.kernel.org/pub/linux/utils/raid/$pkgname/$pkgname-$pkgver.tar.gz
	$pkgname.initd
	$pkgname.confd
	$pkgname-raid.initd
	sysmacros.patch
	time64.patch
	"

build() {
	make
}

package() {
	make DESTDIR="$pkgdir" install
	install -Dm644 $pkgname.conf-example "$pkgdir"/etc/$pkgname.conf
	install -Dm644 "$srcdir"/$pkgname.confd "$pkgdir"/etc/conf.d/$pkgname
	install -Dm755 "$srcdir"/$pkgname.initd "$pkgdir"/etc/init.d/$pkgname
	install -Dm755 "$srcdir"/$pkgname-raid.initd "$pkgdir"/etc/init.d/$pkgname-raid
}

udev() {
	pkgdesc="$pkgdesc (udev rules)"
	install_if="eudev $pkgname=$pkgver-r$pkgrel"

	mkdir -p "$subpkgdir"
	mv "$pkgdir"/lib "$subpkgdir"/
}

misc() {
	pkgdesc="$pkgdesc (misc scripts)"
	depends="$pkgname bash"

	install -Dm755 "$builddir"/misc/mdcheck "$subpkgdir"/usr/sbin/mdcheck
	install -Dm755 "$builddir"/misc/syslog-events "$subpkgdir"/usr/sbin/handle-mdadm-events
}

sha512sums="f9bff760795ba7361f19fd1cbc02efedcdaa4b0125b99cf1369e78f30e5c128126751915fde41407d46c544514d864bf82b508419bc08f1db7aa447557e2ca9e  mdadm-4.1.tar.gz
ca5f4e3ff5b284102b44e818d11622e1889066e3d18edce2d62c1a44ee8f4cfdc0979121c0462a916c638423c5ebc706c46aa996a7c4f68e030589adc62803f4  mdadm.initd
7d45bf489ef93a4f217ffcf72311eb661b8de7fbf63a5344697252c0d3a684b0123ff60efa5f218da4eb4cda7c88d91c2ef3625d5e44a588e3e1210cb60b0ab9  mdadm.confd
37022593ba090eb0690669b99d6386152242c017c1e608cea7b5420b7a6f754b377e916e4f81e2abf9941e791db78b5820e63db0e706d5de8b35e796678e921c  mdadm-raid.initd
47564bba9d45dfb39d63df9e6cd96ad03b37b314e794af180911481feb4e038035aa1ea6d3de2061982f46b51d1a205168f98e6f0a092f55f6f8e760dbabdae6  sysmacros.patch
53c2fe442e8657a7a5a011eab1cd1bfcca6c315ee42e4148a50e1314d238f957e7e722e1264c64c548d398a2c6b10600cccf45a4fb69351d3d9ad403f90c1fde  time64.patch"
