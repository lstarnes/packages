# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=networkmanager
pkgver=1.22.14
pkgrel=3
pkgdesc="Network management daemon"
url="https://wiki.gnome.org/Projects/NetworkManager"
arch="all"
options="!check"  # Requires dbus-python and running DBus server.
license="GPL-2.0+ AND LGPL-2.1+"
depends="dhcpcd iputils ppp wpa_supplicant-dbus"
makedepends="bash bluez-dev curl-dev dbus-dev dbus-glib-dev elogind-dev
	eudev-dev glib-dev gobject-introspection-dev intltool libedit-dev
	libndp-dev libxslt modemmanager-dev ncurses-dev newt-dev nss-dev
	perl polkit-dev ppp-dev py3-pygobject util-linux-dev vala-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang $pkgname-openrc"
source="https://download.gnome.org/sources/NetworkManager/${pkgver%.*}/NetworkManager-$pkgver.tar.xz
	editline.patch
	errno.patch
	musl.patch
	qsort_r.patch
	random.patch
	tests.patch
	reallocarray.patch

	01-org.freedesktop.NetworkManager.rules
	10-openrc-status
	nm.confd
	nm.initd
	"
builddir="$srcdir/NetworkManager-$pkgver"

build() {
	# pppd plugin dir is a huge hack.
	bash ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--disable-json-validation \
		--disable-more-warnings \
		--disable-ovs \
		--disable-qt \
		--disable-static \
		--enable-bluez5-dun \
		--enable-concheck \
		--enable-polkit=yes \
		--enable-ppp \
		--enable-vala \
		--with-crypto=nss \
		--with-dbus-sys-dir=/etc/dbus-1/system.d \
		--with-dhcpcd \
		--with-ebpf=yes \
		--with-iptables=/sbin/iptables \
		--with-nmcli=yes \
		--with-nmtui \
		--with-pppd-plugin-dir=/usr/lib/pppd/$(ls -1 /usr/lib/pppd | head) \
		--with-session-tracking=elogind \
		--with-suspend-resume=elogind \
		--without-dhclient \
		--without-libaudit \
		--without-libpsl \
		--without-netconfig \
		--without-systemd-journal
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install

	install -D -m755 "$srcdir"/nm.initd "$pkgdir"/etc/init.d/NetworkManager
	install -D -m644 "$srcdir"/nm.confd "$pkgdir"/etc/conf.d/NetworkManager
	install -D -m600 -t "$pkgdir"/usr/share/polkit-1/rules.d/ \
		"$srcdir"/01-org.freedesktop.NetworkManager.rules
	install -D -m755 -t "$pkgdir"/etc/NetworkManager/dispatcher.d/ \
		"$srcdir"/10-openrc-status
	install -d "$pkgdir"/etc/NetworkManager/system-connections
	touch "$pkgdir"/etc/NetworkManager/system-connections/.keepdir
}

openrc() {
	default_openrc
	mkdir -p "$subpkgdir"/etc/NetworkManager/dispatcher.d
	mv "$pkgdir"/etc/NetworkManager/dispatcher.d/10-openrc-status \
		"$subpkgdir"/etc/NetworkManager/dispatcher.d/
}

sha512sums="81ccb634c3583406d52c159d1b0903f98c2732f9cd6962f3d6d71940d05cba32e262219de4f09c0cc687beff57bd7ba425f06d9a9c1bfa60aef11d427e91f453  NetworkManager-1.22.14.tar.xz
ecd9cb920a125e0e3c4b8c96048ca3ac24490fdd453f0525ecaccf688687692e7a5feaf87eeaf97a2dfb405b2e0db8743114510e30f00f7ae119dc9b9551e7d7  editline.patch
b0b85294c19510893ba30cf3d0e1984430c451273d4eb545760de52c69866a5ed0385f9c14a9bc180da01daad7d1c0da221101e7a826bc5be53c2724c9990d95  errno.patch
7b3cfbea8b80f832862d79fce9f89f604b457744324a8ae6b428cf3cb42ec2536d3c5784303d140d50c5a2cd8caf91d04f5096ef420a57a761e412bf2256bd5a  musl.patch
5142bf14ac5574a3ae4bc8753055c534702892871ca45a1bc4d48d4d5c07d11bd54e52861458a673c328d129cdd6f1f749a9546eba1c16fea5173f92840506de  qsort_r.patch
d81a962e32e696ca713dfcf1f8dbd9a146f94473c391f1977198ca67e11a6d427a265bacbe43197e42b442cfaa882e9fd2bba7548872586d53d3caca76263e79  random.patch
602859c1c7b63984b3b9d9597772e4bff496b3838eb0131ad1d707ae9226c5bdd703080683e48bc93b9c1a6572505dad0332d63c57e6320e63c011931877233a  tests.patch
d1250987879d9d660932065953d35c620a15b7242f07e676855b68d0698348e8f54dce43fa67c53f470a10f7c8a16a9834af82ba9299da0e74c7687073e89a6f  reallocarray.patch
5ae288073ddc818cc5a0103a9320ebcbd80bccbba9f823335c1c602c25e48e041990370e6d091d9796e543222a7a58625ce832022d450b9a9dd49df4ed6e1ed9  01-org.freedesktop.NetworkManager.rules
26f962cea0b6a75549d16c19a71e3559239b08b001927882702f5d9355a2cc95f7b13c581439f758a950d9a5bfb73b86ba61a5ffb22c9abe19f5b05fe5f5834a  10-openrc-status
f8c9331834cbc66ab0e4e6f4a120fde6a08a435d680a1e1087718fdbb2b9843a313e7ec30b7008822999dafd98e152aa7d2a96f87320ba9c91c3adb63e0b7c9a  nm.confd
9a5ab7d317797489a555b185d103d3c526cd6963c6586da2f5212b41e68a9bf5dedd52d1fe58718fb686379b6e2434924d06d7da8070a62a3ec3e5626ab90d14  nm.initd"
