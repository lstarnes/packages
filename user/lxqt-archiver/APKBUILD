# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=lxqt-archiver
pkgver=0.9.0
_lxqt=1.4.0
_lxqt_build=0.13.0
pkgrel=0
pkgdesc="Archive management utility for the LXQt desktop"
url="https://lxqt.github.io/"
arch="all"
options="!check"  # No test suite
license="GPL-2.0+"
depends=""
makedepends="cmake extra-cmake-modules json-glib-dev libfm-qt-dev>=$_lxqt
	lxqt-build-tools>=$_lxqt_build qt5-qttools-dev qt5-qtx11extras-dev"
source="https://github.com/lxqt/lxqt-archiver/releases/download/$pkgver/lxqt-archiver-$pkgver.tar.xz"
builddir="$srcdir/$pkgname-$pkgver/build"

prepare() {
	mkdir -p "$builddir" && cd "$builddir"/..
	default_prepare
}

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		..
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest ..
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="42ee097ae7fb17dbd00d9b94e8673664911c73bb5f353d08177c78d548eb24c39aa1daf57347eeefcb46ec63a8702c37f63ca2b42fd22bc85aed8f7a41585f3c  lxqt-archiver-0.9.0.tar.xz"
