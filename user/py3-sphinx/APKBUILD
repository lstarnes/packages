# Contributor: Brandon Bergren <git@bdragon.rtk0.net>
# Maintainer: Brandon Bergren <git@bdragon.rtk0.net>
pkgname=py3-sphinx
_pkgname=Sphinx
_p="${_pkgname#?}"
_p="${_pkgname%"$_p"}"
pkgver=2.1.2
pkgrel=0
pkgdesc="Sphinx Python Documentation Generator"
url="https://pypi.python.org/pypi/Sphinx"
arch="noarch"
options="!check" # Some latex tests failing on 2.0.0 due to translation bug, see https://github.com/sphinx-doc/sphinx/pull/6218 
# Main software is BSD-2-Clause.
license="BSD-2-Clause AND BSD-3-Clause AND MIT"
depends="python3 py3-alabaster py3-babel py3-pygments py3-docutils py3-imagesize py3-jinja2 py3-packaging py3-pyparsing py3-requests py3-snowballstemmer py3-tz py3-sphinxcontrib-applehelp py3-sphinxcontrib-devhelp py3-sphinxcontrib-htmlhelp py3-sphinxcontrib-jsmath py3-sphinxcontrib-qthelp py3-sphinxcontrib-serializinghtml imagemagick"
checkdepends="py3-pytest py3-docutils py3-html5lib"
makedepends="python3-dev"
source="$pkgname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/$_p/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	python3 setup.py build
}

check() {
	# settings borrowed from tox.ini.
	PYTHONPATH="build/lib:$PYTHONPATH" PYTHONWARNINGS="all,ignore::ImportWarning:pkgutil,ignore::ImportWarning:importlib._bootstrap,ignore::ImportWarning:importlib._bootstrap_external,ignore::ImportWarning:pytest_cov.plugin,ignore::DeprecationWarning:site,ignore::DeprecationWarning:_pytest.assertion.rewrite,ignore::DeprecationWarning:_pytest.fixtures,ignore::DeprecationWarning:distutils" pytest --durations 25
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

doc() {
	local docdir="$subpkgdir/usr/share/doc/$pkgname"
	mkdir -p "$docdir"
	cp -R docs/* "$docdir"
	cp *.txt "$docdir"

	local licdir="$subpkgdir/usr/share/licenses/$pkgname"
	mkdir -p "$licdir"
	rm -f licenses/docutils.conf
	cp licenses/* "$licdir"
}

sha512sums="22696cd143b75c497c36e08b81b10981e80ad10a4f9f42a29165bfed18bacbc33dc2f477926fe795faf9918dce9d283cd1acf03f7289bd50fa93a86e7979d9f1  py3-sphinx-2.1.2.tar.gz"
