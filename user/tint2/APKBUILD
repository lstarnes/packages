# Contributor: Mari Hahn <mari.hahn@wwu.de>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=tint2
pkgver=17.0.2
pkgrel=0
pkgdesc="Simple, unintrusive panel/taskbar"
url="https://gitlab.com/o9000/tint2"
arch="all"
options="!check"  # No test suite.
license="GPL-2.0-only AND GPL-2.0+ AND MIT AND Custom:zlib-optional-acknowledgement"
depends=""
makedepends="cmake imlib2-dev glib-dev pango-dev cairo-dev
	libxcomposite-dev libxdamage-dev libxinerama-dev libxrandr-dev
	gtk+3.0-dev librsvg-dev startup-notification-dev linux-headers"
subpackages="$pkgname-doc $pkgname-lang"
source="https://gitlab.com/o9000/$pkgname/-/archive/v$pkgver/$pkgname-v$pkgver.tar.bz2"
builddir="$srcdir/$pkgname-v$pkgver"

build() {
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_SYSCONFDIR=/etc \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-Bbuild
	make -C build
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="2b309fd1ab01e5176cc95e0651f28224b158c2bafd58cfff19840b0f8e880e9f4dcefeff32bc66d5fac11d932d4ee668ea7ddd7bab860f9edcfcdb7074e0e0ed  tint2-v17.0.2.tar.bz2"
