# Contributor: Laurent Bercot <ska-adelie@skarnet.org>
# Maintainer: Laurent Bercot <ska-adelie@skarnet.org>
pkgname=netqmail
pkgver=1.06
pkgrel=10
pkgdesc="The qmail mail transfer agent (community version)"
url="http://www.netqmail.org/"
arch="all"
license="Public-Domain"
options="suid !check" # suid programs (qmail-queue); no test suite
depends="execline s6 s6-networking smtpd-starttls-proxy ca-certificates !ssmtp"
makedepends="groff utmps-dev"
subpackages="$pkgname-doc $pkgname-openrc"
provider_priority=1
provides=/usr/sbin/sendmail
install="$pkgname.post-install $pkgname.pre-deinstall"
source="http://www.netqmail.org/$pkgname-$pkgver.tar.gz
	0001-DESTDIR.patch
	0002-qbiffutmpx-20170820.patch
	0003-qmailremote-20170716.patch
	0004-notifyfd.patch
	0005-CVE-2005-1513.patch
	rename-mbox-man.patch
	qmail.run
	smtpd.run
	smtpd-notls.run
	$pkgname.confd
	$pkgname.initd"

makeservicedir()
{
	mkdir -p -m 0755 "$1"/log "$1"/env
	{
		echo '#!/bin/execlineb -P'
		echo
		echo 's6-setuidgid qmaill'
		echo 's6-envdir ../env'
		echo 'importas -u IP IP'
		echo 'exec -c'
		echo "s6-log t $4"
	} > "$1"/log/run
	echo "$2" > "$1"/notification-fd
	cp -f "$3" "$1"/run
	
	mkdir -p -m 3730 "$1"/event
	mkdir -p -m 0700 "$1"/supervise
	touch "$1"/supervise/lock "$1"/supervise/death_tally
	mkfifo -m 0600 "$1"/supervise/control
	dd if=/dev/zero of="$1"/supervise/status bs=35 count=1
	if test $5 -eq 0 ; then
		echo /var/qmail/bin:/usr/bin:/usr/sbin:/bin:/sbin > "$1"/env/PATH
		echo "$2" > "$1"/env/QMAIL_NOTIFY_FD
	else
		echo 110 > "$1"/env/UID
		echo 200 > "$1"/env/GID
		echo > "$1"/env/GIDLIST
		if "$6" ; then
			echo 116 > "$1"/env/TLS_UID
			echo 200 > "$1"/env/TLS_GID
			echo /etc/ssl/certs > "$1"/env/CADIR
		fi
	  	mkdir -p -m 0755 "$1"/data/rules/ip6/::_0
	  	mkdir -p -m 0755 "$1"/data/rules/ip4/0.0.0.0_0
	  	touch "$1"/data/rules/ip6/::_0/allow "$1"/data/rules/ip4/0.0.0.0_0/allow
	fi
	chmod 0755 "$1"/run "$1"/log/run
}

prepare() {
	default_prepare
	mv "$builddir"/mbox.5 "$builddir"/qmail-mbox.5
}

build() {
        echo "$CC $CFLAGS" > conf-cc
        echo "$CC $LDFLAGS -s -static" > conf-ld
        echo "$CC $LDFLAGS" > conf-ldi  # because fakeroot doesn't work with static programs
        echo 022 > conf-patrn
        echo /var/qmail > conf-qmail
        echo 255 > conf-spawn
        { echo alias; echo qmaild; echo qmaill; echo root; echo qmailp; echo qmailq; echo qmailr; echo qmails; } > conf-users
        { echo qmail; echo nofiles; } > conf-groups
        make
}

package() {
	mkdir -p -m 0755 "$pkgdir"/var/qmail/services "$pkgdir"/var/log/qmail "$pkgdir"/usr/bin "$pkgdir"/usr/sbin "$pkgdir"/usr/share/doc "$pkgdir"/etc/qmail/services "$pkgdir"/etc/conf.d "$pkgdir"/etc/init.d
	chown qmaill:qmaill "$pkgdir"/var/log/qmail
	chmod 2700 "$pkgdir"/var/log/qmail
	cp -f "$srcdir/$pkgname".confd "$pkgdir/etc/conf.d/$pkgname"
	cp -f "$srcdir/$pkgname".initd "$pkgdir/etc/init.d/$pkgname"
	chmod 0755 "$pkgdir/etc/init.d/$pkgname"

	env DESTDIR="$pkgdir" make setup install
	ln -s ../../var/qmail/bin/sendmail "$pkgdir"/usr/sbin/
	ln -s ../../var/qmail/control "$pkgdir"/etc/qmail/control
	rm -rf "$pkgdir"/var/qmail/boot "$pkgdir"/var/qmail/man/cat?
	rm -f "$pkgdir"/var/qmail/bin/qbiff "$pkgdir"/var/qmail/man/man1/qbiff.*
	mv -f "$pkgdir"/var/qmail/man "$pkgdir"/usr/share/man
	mv -f "$pkgdir"/var/qmail/doc "$pkgdir/usr/share/doc/$pkgname-$pkgver"
	echo 255 > "$pkgdir"/var/qmail/control/concurrencylocal
	echo 255 > "$pkgdir"/var/qmail/control/concurrencyremote
	makeservicedir "$pkgdir"/var/qmail/services/qmail 7 "$srcdir"/qmail.run 'n20 s1000000 /var/log/qmail' 0 false
	makeservicedir "$pkgdir"/etc/qmail/services/smtpd-skeleton 3 "$srcdir"/smtpd.run '/var/log/smtpd-$IP' 4 true
	makeservicedir "$pkgdir"/etc/qmail/services/smtpd-skeleton-notls 3 "$srcdir"/smtpd-notls.run '/var/log/smtpd-$IP' 4 false
}

sha512sums="de40a6d8fac502bd785010434d99b99f2c0524e10aea3d0f2a0d35c70fce91e991eb1fb8f20a1276eb56d7e73130ea5e2c178f6075d138af47b28d9ca6e6046b  netqmail-1.06.tar.gz
ad126cad5c0d35351919ad87022fd94b910519d91cf82f38c158f423bbfc1b82455844a791ba0c69d347af1a20a86b095bed571f75365a86ea786cbc9c626487  0001-DESTDIR.patch
b3af9c29e6d46daa2a1b9f677c6f32892d5f8c9b8d5c2bdd6f34b106dd5ad41394c05a5ebe145c6e29b4ced4482f08b2d09e7818fd309123c0d087600500e336  0002-qbiffutmpx-20170820.patch
cbebdc72c7cc5c437531c9277534ae552c6d044a83b36e3f3ce60ab5563c55eb814d6c543cc0997abab73075d1b517cc0929dd65674d468d517b0ca38196e2b4  0003-qmailremote-20170716.patch
b32a8a36c8ab8872abd4f1a117482f064a6d631a6bb2ba75cafe61743bef09f923d26935d9514eec33a7dec5aeb3d0b517d677e55924859d2db5233bc11f9f11  0004-notifyfd.patch
ac8406c1d16ce2e55e47bc83ca6e095833a54de73cecee222cad3fcececa518386b95a11cb0c9c2dcc6851bae28aa539b11069305aa887a291177bf177ee7b01  0005-CVE-2005-1513.patch
6ab1751b6ae1b932505a11ebaa4661edf9dd1b64da66a117c6b97c70cee7e429aaf8db98a3cabf25072d23ef39dc82f586a9adfe848635f6f1bdb0b20abed509  rename-mbox-man.patch
954a905bac5e3bc49f180dc0de7f6ee4c4ae8f94dd400ee4b06d3c944f1ff1cfc44bddccb07ae439f2523ad06fcb89023e57d091737da88f836013757794e931  qmail.run
8a887769fc8c5e1abbc56b72c913b435874549c213a10219124c554640eff09ace0a00a894907d633bdf424c7f351064b94e9e12635f725edc700db8cbf25ccd  smtpd.run
721f4ae97ca302e6e1a8a8f685aed8542e408f580b51362006b385a48cbbec185080048bc3ef0953d10af1aaa15fa661dced930f321d594cbbcda8fe19f49abb  smtpd-notls.run
57c30023fa479b88923712c5688469d61f70af3fc7c0d48eb445696f3b8a67e9279814932539e6958660d4ddecdce3dc804fbbde9613dab74001de25f9ef9bad  netqmail.confd
e19719df558655e0e288bd188ed7669bd1505701680e15612ce66867bda93ea7297ecf70172d7f0245351422c94672c795ea175c3aa3b7a876434fa458bdab68  netqmail.initd"
