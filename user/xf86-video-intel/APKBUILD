# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=xf86-video-intel
verbase=2.99.917
pkgver=${verbase}_git20200224
pkgrel=0
pkgdesc="Legacy X.Org driver for Intel graphics cards"
url="https://xorg.freedesktop.org"
arch="pmmx x86_64"
options="!check"  # No test suite.
license="MIT"
depends="mesa-dri"
makedepends="xorg-server-dev libxi-dev libdrm-dev mesa-dev libxvmc-dev
	xcb-util-dev eudev-dev util-macros autoconf automake libtool xorgproto
	libxv-dev
	"
subpackages="$pkgname-doc"
source="https://dev.sick.bike/dist/$pkgname-$pkgver.tar.gz"
giturl="https://gitlab.freedesktop.org/xorg/driver/xf86-video-intel.git"
reporev="f66d39544bb8339130c96d282a80f87ca1606caf"

prepare() {
	default_prepare
	autoreconf -vif
}

build() {
	export LDFLAGS="$LDFLAGS -Wl,-z,lazy"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--enable-xvmc \
		--disable-selective-werror \
		--with-default-dri=3
	make
}

package() {
	make DESTDIR="$pkgdir" install
	install -Dm644 COPYING "$pkgdir"/usr/share/licenses/$pkgname/COPYING
	rm "$pkgdir"/usr/libexec/xf86-video-intel-backlight-helper
}

sha512sums="cb853076238cbde61faeb7dd1a07c5edc5325361dc3109acd9c142c1bf66566ee8c19f8e709bde34456e4198353ac72d337bc6a43dbcc80a4546762541be6eb9  xf86-video-intel-2.99.917_git20200224.tar.gz"
