# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=alpine
pkgver=2.26
pkgrel=0
pkgdesc="Terminal-based email client"
url="https://alpineapp.email/"
arch="all"
license="Apache-2.0"
depends=""
makedepends="aspell-dev krb5-dev linux-pam-dev ncurses-dev openldap-dev
	openssl-dev"
subpackages="$pkgname-doc"
# NOTE: "old" releases are moved elsewhere, so URLs will break often.
# The author was contacted about this on 2022-09-30.
#source="https://alpineapp.email/alpine/release/src/$pkgname-$pkgver.tar.xz"
source="$pkgname-$pkgver.tar.gz::https://repo.or.cz/alpine.git/snapshot/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/$pkgname-v$pkgver"

# secfixes:
#   2.25-r0:
#     - CVE-2021-38370

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="7ca4d5fc7cc9b5fc63b0cb341fa161274c7f724d3f812a7f94dcfe60d678665f0fbce5b671fa26d4da822f09ac58978a3f6385a94c8f3dc9b16bd8fa66a49634  alpine-2.26.tar.gz"
