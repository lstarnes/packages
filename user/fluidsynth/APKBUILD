# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=fluidsynth
pkgver=2.1.5
pkgrel=0
pkgdesc="Real-time, software synthesiser based on SoundFont 2"
url="http://www.fluidsynth.org/"
arch="all"
license="LGPL-2.1+"
depends=""
makedepends="alsa-lib-dev cmake dbus-dev glib-dev libsndfile-dev pulseaudio-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-libs"
source="fluidsynth-$pkgver.tar.gz::https://github.com/FluidSynth/fluidsynth/archive/v$pkgver.tar.gz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DLIB_INSTALL_DIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="171ee6b1983ab9636086fb414ae464508f7a9a412cd2c1e7800a19a84d3dff21bbd86d1971f9bb5985e977f808b60e0d0cfbde4551b85fcc4aae4928a1abadbc  fluidsynth-2.1.5.tar.gz"
