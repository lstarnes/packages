# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kblocks
pkgver=22.04.2
pkgrel=0
pkgdesc="Falling blocks game"
url="https://www.kde.org/applications/games/kblocks/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev kconfig-dev
	kconfigwidgets-dev kcoreaddons-dev kcrash-dev kdoctools-dev ki18n-dev
	kdbusaddons-dev kwidgetsaddons-dev kxmlgui-dev libkdegames-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kblocks-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	# All other tests require a running X11 session.
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -R 'UnitTest-basic'
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="bc8922f0c5408867f105c4f9e6987525f88fd2092631e546e599aa25043adc3cc06103b882f44ee47362576cf12ed9dc8c1ff6ec5d5df349c67bea5b42663e19  kblocks-22.04.2.tar.xz"
