# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Bartłomiej Piotrowski <bpiotrowski@alpinelinux.org>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=wayland
pkgver=1.21.0
pkgrel=0
pkgdesc="A computer display server protocol"
url="https://wayland.freedesktop.org"
arch="all"
license="MIT"
depends=""
depends_dev="libffi-dev expat-dev"
makedepends="$depends_dev doxygen xmlto graphviz grep libxml2-dev bash meson"
subpackages="$pkgname-dev"
replaces="wayland-libs-client wayland-libs-cursor wayland-libs-server"
source="https://gitlab.freedesktop.org/wayland/wayland/-/releases/$pkgver/downloads/$pkgname-$pkgver.tar.xz"

build() {
	meson \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--buildtype=plain \
		-Ddocumentation=false \
		build

	ninja -C build
}

check() {
	export XDG_RUNTIME_DIR=$(mktemp -d)
	ninja -C build test
	rm -fr "${XDG_RUNTIME_DIR}"
}

package() {
	DESTDIR="$pkgdir" ninja -C build install
}

dev() {
	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/bin "$pkgdir"/usr/share \
		"$subpkgdir"/usr
	default_dev
}

sha512sums="5575216d30fdf5c63caa6bcad071e15f2a4f3acb12df776806073f65db37a50b5b5b3cc7957c5497636f4ac01893e2eaab26e453ded44b287acde01762f5fdc3  wayland-1.21.0.tar.xz"
