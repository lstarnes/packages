# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kalzium
pkgver=22.04.2
pkgrel=0
pkgdesc="Periodic table of elements (PSE) with calculators"
url="https://www.kde.org/applications/education/kalzium/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtscript-dev
	eigen-dev qt5-qtdeclarative-dev qt5-qtsvg-dev karchive-dev kconfig-dev
	kcoreaddons-dev kdoctools-dev ki18n-dev kdelibs4support-dev khtml-dev
	kparts-dev kplotting-dev solid-dev kunitconversion-dev
	kwidgetsaddons-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kalzium-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DEIGEN3_INCLUDE_DIR=/usr/include/eigen3 \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="161dddba0eafc00ae713381a983b3719e5a09363d834b5b62dfc030d99c0e92da1ace517481a0675f72ca13c94c07df78e87ced699af100a4e8067530a020267  kalzium-22.04.2.tar.xz"
