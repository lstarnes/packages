# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=libmatemixer
pkgver=1.26.1
pkgrel=0
pkgdesc="Sound mixer library for the MATE desktop environment"
url="https://mate-desktop.org"
arch="all"
license="LGPL-2.0+"
depends=""
makedepends="alsa-lib-dev intltool pulseaudio-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang
	$pkgname-alsa $pkgname-pulse"
source="https://pub.mate-desktop.org/releases/${pkgver%.*}/libmatemixer-$pkgver.tar.xz"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

alsa() {
	pkgdesc="$pkgdesc (ALSA backend)"
	install_if="$pkgname=$pkgver-r$pkgrel alsa-lib"
	mkdir -p "$subpkgdir"/usr/lib/$pkgname
	mv "$pkgdir"/usr/lib/$pkgname/$pkgname-alsa.so "$subpkgdir"/usr/lib/$pkgname
}

pulse() {
	pkgdesc="$pkgdesc (PulseAudio backend)"
	install_if="$pkgname=$pkgver-r$pkgrel pulseaudio"
	mkdir -p "$subpkgdir"/usr/lib/$pkgname
	mv "$pkgdir"/usr/lib/$pkgname/$pkgname-pulse.so "$subpkgdir"/usr/lib/$pkgname
}

sha512sums="a67a63bbf76b69ba78a537f7690230f45c5875b4358991e73604aacf001baa40ee994101d486218be8000be43be6561b6f25f4f38ae00310c5a08affb6dafbdb  libmatemixer-1.26.1.tar.xz"
