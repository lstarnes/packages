# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=node
pkgver=18.15.0
pkgrel=0
pkgdesc="JavaScript runtime"
url="https://nodejs.org/"
arch="all !ppc"  # #837
options="net"  # Required in check()
license="MIT AND ICU AND BSD-3-Clause AND BSD-2-Clause AND ISC AND Public-Domain AND Zlib AND Artistic-2.0 AND Apache-2.0 AND CC0-1.0"
depends=""
makedepends="c-ares-dev http-parser-dev icu-dev libexecinfo-dev libuv-dev
	nghttp2-dev openssl-dev python3 zlib-dev samurai"
subpackages="$pkgname-dev $pkgname-doc"
source="https://nodejs.org/download/release/v$pkgver/node-v$pkgver.tar.xz
	pmmx-test.patch
	pmmx-time64.patch
	zlib-version-regex.patch
	"
builddir="$srcdir/$pkgname-v$pkgver"

# secfixes:
#   10.16.3-r0:
#     - CVE-2019-9511
#     - CVE-2019-9512
#     - CVE-2019-9513
#     - CVE-2019-9514
#     - CVE-2019-9515
#     - CVE-2019-9516
#     - CVE-2019-9517
#     - CVE-2019-9518
#   10.21.0-r0:
#     - CVE-2020-7598
#     - CVE-2020-8174

build() {
	case "${CARCH}" in
		armv7|ppc|pmmx) # ld: final link failed: memory exhausted
			export CXXFLAGS="${CXXFLAGS} -g0";
			;;
	esac

	python3 configure.py \
		--prefix=/usr \
		--shared-zlib \
		--shared-openssl \
		--shared-cares \
		--shared-nghttp2 \
		--ninja \
		--openssl-use-def-ca-store \
		--with-icu-default-data-dir=$(icu-config --icudatadir) \
		--with-intl=system-icu \
		--without-corepack \
		--without-npm

	make BUILDTYPE=Release DESTDIR="$pkgdir"
}

check() {
	make DESTDIR="$pkgdir" test-only \
		${_skip:+CI_SKIP_TESTS="$_skip"}
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="e41fcda469809186fd724ef4691e25f4a5bd81357ee99acf3d7faa1190a69c19cb62bd14aea199ca6f8b5cf9687af7d898cdf605ea2414d2c04db87ddb3b4dc8  node-v18.15.0.tar.xz
277e226f3906f791bae6aedd0b74b0e2c52b6154eb2dc0c568417ad94a0722078e4fbbbe15c59d4ba0b59cdb4ad45b5e9620f14d75694a15531857cd29aa044a  pmmx-test.patch
bf78e52c60b4567854eaa9d9433ade8a318a356cb326dded99e800df35a9f475390a0cf8b0c8e595bbdb3702838eafe91801cd646576aa7fa7966b37d794e380  pmmx-time64.patch
45d899bd62e39762fde7e9743efcc6dc032161ae087099da8eecebc84f3eaab87eecf00cbc5861f686a45332224025af172ab00ce966771dccf2e925ca48bc6a  zlib-version-regex.patch"
