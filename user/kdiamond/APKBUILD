# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kdiamond
pkgver=22.04.2
pkgrel=0
pkgdesc="Three-in-a-row game"
url="https://games.kde.org/game.php?game=kdiamond"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kcoreaddons-dev
	kconfig-dev kcrash-dev kdbusaddons-dev kdoctools-dev kwidgetsaddons-dev
	ki18n-dev kconfigwidgets-dev kxmlgui-dev knotifications-dev
	knotifyconfig-dev libkdegames-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kdiamond-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="6d88d48162df4d7a5819de6c2419e781a316a4c5f90c6b8ad8523ca86f57fa74d5b7fdae184b68a5fd5e999827149f2dbe04f50ccb20df5498c854470f17da45  kdiamond-22.04.2.tar.xz"
