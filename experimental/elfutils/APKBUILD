# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=elfutils
pkgver=0.176
pkgrel=0
pkgdesc="A collection of utilities and DSOs to handle ELF files and DWARF data"
url="https://sourceware.org/elfutils/"
arch="all"
license="GPL-3.0+ AND (GPL-2.0+ or LGPL-3.0+) AND LGPL-2.1+"
depends=""
makedepends="argp-standalone bsd-compat-headers bzip2-dev flex
	fts-dev musl-obstack-dev xz-dev zlib-dev"
subpackages="$pkgname-dev $pkgname-lang $pkgname-tools"
source="https://sourceware.org/elfutils/ftp/$pkgver/$pkgname-$pkgver.tar.bz2
	extmatch.patch
	fix-aarch64_fregs.patch
	fix-uninitialized.patch
	musl-cdefs.patch
	musl-fts-obstack.patch
	musl-macros.patch
	musl-qsort_r.patch
	musl-strerror_r.patch
	musl-strndupa.patch
	test-disable-backtrace.patch
	test-disable-biarch.patch
	error.h"

# Internal only - should not be exposed to other packages
somask="libebl_aarch64-$pkgver.so
	libebl_alpha-$pkgver.so
	libebl_arm-$pkgver.so
	libebl_bpf-$pkgver.so
	libebl_i386-$pkgver.so
	libebl_ia64-$pkgver.so
	libebl_m68k-$pkgver.so
	libebl_ppc-$pkgver.so
	libebl_ppc64-$pkgver.so
	libebl_riscv-$pkgver.so
	libebl_s390-$pkgver.so
	libebl_sh-$pkgver.so
	libebl_sparc-$pkgver.so
	libebl_tilegx-$pkgver.so
	libebl_x86_64-$pkgver.so"

prepare() {
	default_prepare
	autoreconf -vif
	cp "$srcdir"/error.h lib
	cp "$srcdir"/error.h src
}

build() {
	export CFLAGS="$CFLAGS -Wno-error -g"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

tools() {
	pkgdesc="$pkgdesc (command-line tools)"
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/eu-* "$subpkgdir"/usr/bin
}

sha512sums="7f032913be363a43229ded85d495dcf7542b3c85974aaaba0d984228dc9ac1721da3dc388d3fa02325a80940161db7e9ad2c9e4521a424ad8a7d050c0902915b  elfutils-0.176.tar.bz2
c3676b0b8b40d6e437f5d88c0d658dc0829ec97d1955e4bbf96f4cff9ee9001b8f2f06174450ae7aa5858b91c7905bdbd49073b561c346416132338a9c94731b  fix-aarch64_fregs.patch
8b421bc411fd240ada1d6f4010e222cb430bceebea613b79dc96d8244835b87272e9964f95c6ac54d054978fdc170f3b852606c207804df7fc261bb544f61c53  fix-uninitialized.patch
3d8c7aee5d509486b680c1eb8903052060386520e8576068a8144019857933b86bd708c2a0a44c383667c4205888435402733dbd8ea0a88c32223a639b0404f0  extmatch.patch
8c5c2fab616a01df7b57c9338122eb054503c0d76808d1914ae112106c2e73b9cef517719242b8ee3a78479e73e97187fe42d6e897e0b822e5a0a9e30b2246b9  musl-cdefs.patch
7a22dc1a3423d1e4dd8f1ddbc5c1e1ee87db679d7b9fba0f7e695c207c9fefef5cacdee3d7c5b64ff2751a1d5b155751f0e77ee9af844048a32a8dd890623ef2  musl-fts-obstack.patch
37a8cd13262115b64b51d47e9d5667185155d1b1159aa333ac34df7cc6d7d71d9c809a9fd99094e5f4102c6535a32f75159ac80f4986e029a0e2cd49b76884d3  musl-macros.patch
38e5403d645fe2da4df9425c6a3fb00dc8ca8016ed86a19482de3d6a2e4200f3953e628b0a6bc33d3d3ce25733b7171ad887448e21981c0ae39343c163fdff41  musl-qsort_r.patch
a0d986100c8ff2ef0595645ec1b2eeb1d517b7442aef5f349ebf27fcb66c76e51fadeda25bed5f04b4bb16a61aa23ac6e86a1f34a0087d2136acf0f64c3fa4d1  musl-strerror_r.patch
83ad42f672e1d5ca479bc44166c423624ab14d70f74bd6f703ef5fd98694ae201db975026ae8bd507d87b038e3403d878c90da40cfcf6d2364bbd3cad8cedb5b  musl-strndupa.patch
edfeaad36b52997a43612b8bc2d26bcde7d2edb6b1b932e42f88a859c46747eb57a4ae963a98157e60d89f3d4f04efe50a69ea91cc0552cb5b0db17e8cc833c1  test-disable-backtrace.patch
d3263c42b1638a416e95c2b8e68b15a9723e748490c8eca727da94bf7bb3dd8389222dd01bf69612ba45a20114ee1427d77935c41436d1ca9180fcfd71d3cdfe  test-disable-biarch.patch
b33d7f210b9652b7b919afb32b4674ca125b660bf5f81fafb4e4e8405ea16be74ce85f653e6c0ac83e5fff1b192e82e273c5b5baa3802fdc7602edfa1086936d  error.h"
